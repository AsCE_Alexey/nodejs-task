const http = require('http')
const events = require('events')
const server = http.createServer()

var MongoClient = require('mongodb').MongoClient,
  mongoUrl = 'mongodb://192.168.88.233/project_test'
var emitter = new events()
var mongodb;
var connect = false;

function connectToMongo(){
  MongoClient.connect(mongoUrl, {server: {auto_reconnect: true}})
  .then(db => {
    mongodb = db
    connect = true
    console.log("connect to mongo")
  })
  .catch(err => {
    console.log("can not connect to mongodb: " + err.message)
    setTimeout(connectToMongo, 5000)
  })
}

connectToMongo()

server.on('request', function(req, res) {
  var url = req.url
  var eventName = ""
  if(url == '/users') {
    console.log("users request")
    eventName = "users"
  } else if(url == '/posts') {
    console.log("posts request")
    eventName = "posts"
  }
  req.resume()
  req.on('end', function() {
    res.setHeader("Access-Control-Allow-Origin", "*")
    res.setHeader("Access-Control-Allow-Headers", "Content-Type")
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTION")
    if(eventName == "") {
      console.log("Unknown request")
      res.writeHead(404)
      res.end()
      return
    }
    if(emitter.listeners(eventName).length == 0) {
      if (!connect) {
        console.log("db is not connected")
        res.writeHead(500)
        res.end("Db is not connected")
        return
      }
      new Promise((resolve, reject) => {
        setTimeout( () => {
          reject(new Error("Db is not connected"))
        }, 3000)
        var col = mongodb.collection(eventName)
        col.find().toArray().then(result => {
          resolve(result)
        })
        .catch(err => {
          reject(err)
        })
      })
      .then(result => {
        emitter.emit(eventName, null, JSON.stringify(result))
      })
      .catch(err => {
        console.log("Error request: " + err.message)
        emitter.emit(eventName, err.message)
      })
    }
    emitter.once(eventName, (err, data) => {
      if(err == null) {
        res.write(data)
      } else {
        res.writeHead(500)
        res.write("Error request: " + err)
      }
      res.end()
    })
    res.on('error', function(err) {
      console.log("error from res " + err.message)
    })
  })
})

server.listen(8080)
